"""
CSCI-603: Graphs
Author: Sean Strout @ RIT CS
        Anurag Malik
        Shraddha Pandya

An implementation of a vertex as part of a graph.

Code taken from the online textbook and modified:

http://interactivepython.org/runestone/static/pythonds/Graphs/Implementation.html
"""

class Vertex:
    """
    An individual vertex in the graph.

    :slots: id:  The identifier for this vertex (user defined, typically
        a string)
    :slots: connectedTo:  A dictionary of adjacent neighbors, where the key is
        the neighbor (Vertex), and the value is the edge cost (int)
    """
    __slots__ = 'id', 'type', 'connectedTo'

    def __init__(self, key, type):
        """
        Initialize a vertex
        :param key: The identifier for this vertex
               type : The type of the vertex
        :return: None
        """
        self.id = key
        self.type = type
        self.connectedTo = {}

    def addNeighbor(self, nbr, weight=0):
        """
        Connect this vertex to a neighbor with a given weight (default is 0).
        :param nbr (Vertex): The neighbor vertex
        :param weight (int): The edge cost
        :return: None
        """
        self.connectedTo[nbr] = weight

    def __str__(self):
        """
        Return a string representation of the vertex and its direct neighbors:

            vertex-id connectedTo [neighbor-1-id, neighbor-2-id, ...]

        :return: The string
        """
        return str(self.id) + ' connectedTo: ' + str([str(x.id) for x in self.connectedTo])

    def getConnections(self, type = 'P'):
        """
        Get the neighbor vertices.
        :return: A list of Vertex neighbors
        """
        res = []
        for x in self.connectedTo:
            if x.getType() == type:
                res.append(x.id)
        return res

    def getType(self):
        """
        Return  the type of the current vertex
        :return:
        """
        return self.type

    def getWeight(self, nbr):
        """
        Get the edge cost to a neighbor.
        :param nbr (Vertex): The neighbor vertex
        :return: The weight (int)
        """
        return self.connectedTo[nbr]

    def getId(self):
        """
        Returns the id of the current vertex
        :return:
        """
        return self.id

    def countNodes(self, visited):
        """
        This method counts the number of type 'C' vertices connected to the current vertex
        and makes recursive calls to its other neighbour vertices of type 'P'

        :param visited:set of all vertices traversed till now
        :return: count : return count of 'C' type nodes at this node and its neighbouring vertices
                 connectedVertices : dictionary of 'C' type (key) and connected vertices (value) on them
        """
        if self.id in visited:
            return 0, {}
        # return if current vertex is a type 'C' vertex
        if self.type == 'C' or len(self.connectedTo) == 0:
            return 0, {}
        count = 0
        connectedVertices = {}
        # add self to set of already visited nodes
        visited.add(self.id)
        for item in self.connectedTo:
            # increment count of type 'C' neighbouring vertices
            # and update dictionary for 'C' type
            if item.getType() == 'C':
                if item.id in connectedVertices:
                    temp = connectedVertices[item.id]
                    temp.append(self.id)
                    connectedVertices[item.id] = temp
                else:
                    connectedVertices[item.id] = [self.id]
                count += 1
            else:
                # skip this neighbour if it is already visited once
                if item.id in visited:
                    continue
                # recursive call to neighbouring vertices
                tempCount, tempList = item.countNodes(visited)
                count += tempCount
                for key in tempList:
                    # add data from dictionary from connected vertices to self dictionary
                    if key in connectedVertices:
                        # key already exist, append new data
                        value = connectedVertices[key]
                        newValue = tempList[key]
                        value.extend(newValue)
                        connectedVertices[key] = value
                    else:
                        # create new key-value pair
                        connectedVertices[key] = tempList[key]
        return count, connectedVertices

    def trigger(self, visited):
        """
        This method is built to traverse and thus trigger all the neighbouring vertices of the current node,
        built specially for the holicow functionality.
        :param visited: set of all the vertices that have already been traversed
        :return: None
        """
        if self.id in visited:
            return
        if self.type == 'C':
            return
        # add this vertex to the set of visited vertices
        visited.add(self.id)
        sp = '          '
        # call all the neighbouring vertices of this vertex
        for item in self.connectedTo:
            if item.getType() == 'C':
                print(sp + item.id + " is painted " + self.id + "!")
            else:
                # recursive call if the neighbouring vertex is not already visited
                if item.id not in visited:
                    print(sp + item.id + " paint ball is triggered by " + self.id + " paint ball")
                    item.trigger(visited)