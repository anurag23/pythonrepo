"""
Author: Anurag Malik
        Shraddha Pandya

Implementation of the graph problem and simulation of paint ball and cows
in a field. And finding the best possible paint ball to be triggered aimed
at painting the cows in the fields with the most colors.
"""
import sys
import math
from graph import Graph

def holicow(filename):
    """
    This method is used to simulate the
    scenario of a field in which there are many paintballs and cows,
    description of the locations of paintballs and cows in the field
    are provided in a input file. Triggering a paintball triggers the
    nearby paintball as well as colors the nearby cows also.

    We are required to find the best paintball which should be
    triggered to maximize the number of paint on different
    cows in the field.
    :param filename: file with discription of co-ordinates of
            various paintballs and cows in the field.
    :return: None
    """

    # create a new graph
    field = Graph()
    cow_list = []
    paintball_list = []
    # try and open the input file else raise an exception
    try:
        with open(filename) as file:
            for line in file:
                tokens = line.rstrip('/n').split(' ')
                # check if the next data is for a cow or a paintball
                # append it the corresponding list
                if tokens[0] == "cow":
                    cow_list.append(tokens[1:])
                else:
                    paintball_list.append(tokens[1:])
    except FileNotFoundError:
        print("File not found : ", filename)
        return


    for src in paintball_list:
        # for each paint ball in the list, check if there exist a edge to any other paintball
        for dest in paintball_list:
            if src[0] != dest[0]:
                # radius of the src paintball is more than the distance between src and dest paintball
                connected = math.sqrt(math.pow((float(src[1]) - float(dest[1])), 2) + math.pow((float(src[2]) - float(dest[2])), 2)) <= float(src[3])
                field.addEdge(src[0], dest[0], connected)

        # for each paint ball in the list, check if there exist a edge to any cow type vertex
        for cow in cow_list:
            # radius of the src paintball is more than or equal to the distance between the paintball and the cow
            if math.sqrt(math.pow((float(src[1]) - float(cow[1])), 2) + math.pow((float(src[2]) - float(cow[2])), 2)) <= float(src[3]):
                field.addEdge(src[0], cow[0], True, 'C')

    # add any remaining separate paint balls
    for item in paintball_list:
        field.addVertex(item[0])

    # add any separate remaining cows
    for item in cow_list:
        field.addVertex(item[0], 'C')

    # print details of each vertex in the graph
    for vertex in field:
        print(vertex)

    # return if there are no paintballs to color the cows
    if len(paintball_list) < 1:
        print("No paint balls in the field, No cows can be colored")
        return

    # simulate the process of triggering each paintball one by one
    print("\nBeginning simulation...")

    # trigger each paintball once and trace their
    # path to the neighbouring paint balls and cows
    for paintball in field:
        visited = set()
        # cow type vertex, skip it
        if paintball.getType() == 'C':
            continue
        print("Triggering " + str(paintball.getId()) + " paintball...")
        paintball.trigger(visited)

    cows = []
    for cow in cow_list:
        cows.append(cow[0])

    # find
    maxCowsColor(cows, field)

def maxCowsColor(cows, field):
    """
    This method is responsible for finding the best paint ball to be triggered, if any,
    aimed at maximizing the number of colors that the cows are painted with.
    :param cows:list of all the cows
    :param field: reference of the graph/ field
    :return:
    """
    print("\nResults:")
    # return if the cows list is empty
    if len(cows) < 1:
        print("There are no cows in the field")
        return

    maxCount = -1
    bestTrigger = None

    # count the maximum number of paints that the cows can be
    # painted with, starting at each vertex in the graph.
    for paintball in field:
        visited = set()
        # count number of paints and details of colors on cows
        count, list = paintball.countNodes(visited)
        if count > maxCount:
            # update max count and list if
            # this paint ball colors more cows compared to others
            maxCount = count
            maxList = list
            bestTrigger = paintball

    # display the result of above serach.
    if bestTrigger is None or maxCount < 1:
        print("No cows were painted by any starting paint ball")
        return
    else:
        print("Triggering the " + bestTrigger.getId() + " paint ball is the best choice with "\
          + str(maxCount) + " total paint on the cows:")

    space = '           '
    # display the details of colors on all cows
    # when best paint ball found above is triggered
    for item in maxList:
        list = ''
        print(space + item+"'s colors : { ", end = '')
        for x in maxList[item]:
            list += str(x) + ', '
        print(str(list[0:(len(list)-2)]) + " }")
        # remove this cow from the list of all cows
        cows.remove(item)

    for item in cows:
        print(space + item + "'s colors : {}")

def main():
    """
    Program execution starts from this method,
    It calls a holicow method to simulate a
    graph problem of differnt paint balls and cows in a field.
    :return: None
    """
    try:
        # input file name from command line
        file = str(sys.argv[1])
        holicow(file)
    except IndexError:
        print("Usage: python3 holicow.py {filename}")

if __name__ == '__main__':
    main()