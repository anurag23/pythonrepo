__author__ = 'Anurag Malik'
__author__ = 'Shraddha Pandya'

from heap import Heap

class tnode(object):

    __slots__  = ("name", "time", "cost", "min_index", "max_index")

    def __init__(self, name, time, cost, min_index = None, max_index = None):
        '''
        Initialize new task node
        :param name: Name of task
        :param time: time required to complete this job
        :param cost: cost of completing this job
        :param min_index: index in min heap
        :param max_index: index in max heap
        :return: None
        '''
        self.name = name
        self.time = time
        self.cost = cost
        self.max_index =  max_index
        self.min_index = min_index

    def setindex(self, param, index):
        '''
        Set required in index of the current node
        :param param: type of index to be updated
        :param index: new value
        :return: None
        '''
        if param == 'max':
            self.max_index = index
        else:
            self.min_index = index

    def getvalue(self, param):
        '''
        This method is required to get value of the required
        parameter based upon the min or max heap
        :param param: required parameter
        :return: return value
        '''
        if param == 'max':
            return self.cost
        else:
            return self.time

    def popvalues(self):
        '''
        This method returns the values from the current node
        :return: return name, and indices from min and max heaps
        '''
        return [self.name, self.min_index, self.max_index]

class Tasker(object):
    __slots__ = ("minHeap", "maxHeap")

    def __init__(self):
        self.minHeap = Heap("min")
        self.maxHeap = Heap("max", lambda x,y:x>y)

    def read(self):
        '''
        This method reads data from file and either assigns pending
        test to the users or add it to the heap of test for
        two designated users - Cathy and Howard
        :return: None
        '''
        file_name = input("Enter file name :")
        with open(file_name) as file:
            for line in file:
                task = line.rstrip('\n').split(' ')
                # read tokens to decide if it is a new task
                # or a user is ready for new task
                if len(task) == 2:
                    self.assign_task(task)
                else:
                    self.schedule_task(task)

    def schedule_task(self, task):
        '''
        This method adds new task to the min time heap
        for Howard and max cost heap for Cathy.
        :param task: details of new job to be scheduled
        :return: None
        '''
        newTask = tnode(task[0], task[1], task[2])
        print("New job arriving!! Job name : " + str(task[0]) + ", " + \
              str(task[1]) + " hours and $ " + str(float(task[2])))
        # insert new node to min heap for Howard
        self.minHeap.insert(newTask)
        # insert new node to the max heap for Cathy
        self.maxHeap.insert(newTask)

    def assign_task(self, task):
        '''
        Pick new task from min heap for Howard and max heap for Cathy,
        as required. Also, remove the same entry from another heap.
        :param task:
        :return:
        '''
        if task[0] == 'Cathy':
            # pop value from Cathy's max heap
            popped = self.maxHeap.pop()
            if popped is None :
                print("No task to assign")
                return
            print("Cathy starting job : '" + popped[0] + "'")
            # delete same entry from min heap
            self.minHeap.delete(popped[1])
        else:
            # pop value from Howards's min heap
            popped = self.minHeap.pop()
            if popped is None :
                print("No task to assign")
                return
            print("Harold starting job : '" + popped[0] + "'")
            # delete same entry from max heap
            self.maxHeap.delete(popped[2])

def main():
    tasker = Tasker()
    tasker.read()

main()