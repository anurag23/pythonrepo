__author__ = 'Anurag Malik'
__author__ = 'Shraddha Pandya'

class tnode(object):

    __slots__  = ("name", "time", "cost", "min_index", "max_index")

    def __init__(self, name, time, cost, min_index = None, max_index = None):
        self.name = name
        self.time = time
        self.cost = cost
        self.max_index =  max_index
        self.min_index = min_index

    def setindex(self, param, index):
        if param == 'max':
            self.max_index = index
        else:
            self.min_index = index

    def getvalue(self, param):
        if param == 'max':
            return int(self.cost)
        else:
            return int(self.time)

    def popvalues(self):
        return [self.name, self.min_index, self.max_index]