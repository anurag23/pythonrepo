__author__ = 'zjb'
__author__ = 'Anurag Malik'
__author__ = 'Shraddha Pandya'

class Heap(object):
    '''
    Heap that orders by a given comparison function.
    Comparison defaults to less-than for a min heap.
    '''
    __slots__ = ( 'data', 'size', 'param', 'above_fn' )

    def __init__(self, param, above_fn=lambda x,y:x<y):
        '''
        Constructor takes a comparison function.
        :param above_fn: Function that takes in two heap objects and returns true
        if the first arg goes higher in the heap than the second
        '''
        self.data = []
        self.param = param
        self.size = 0
        self.above_fn = above_fn

    def __parent(self,loc):
        '''
        Helper function to compute the parent location of an index
        :param loc: Index in the heap
        :return: Index of parent
        '''
        return (loc-1)//2

    def __left(self,loc):
        '''
        Helper function to compute the parent location of an index
        :param loc: Index in the heap
        :return: Index of parent
        '''
        return loc * 2 + 1

    def __right(self,loc):
        '''
        Helper function to compute the parent location of an index
        :param loc: Index in the heap
        :return: Index of parent
        '''
        return loc * 2 + 2

    def __bubbleUp(self,loc):
        '''
        Starts from the given location and moves the item at that spot
        as far up the heap as necessary
        :param loc: Place to start bubbling from
        '''
        self.data[loc].setindex(self.param, self.size-1)
        parentloc = self.__parent(loc)
        while loc > 0 and \
                 self.above_fn(self.data[loc].getvalue(self.param),self.data[parentloc].getvalue(self.param)):
            (self.data[loc], self.data[parentloc]) = \
                      (self.data[parentloc], self.data[loc])
            self.data[loc].setindex(self.param, loc)
            self.data[parentloc].setindex(self.param, parentloc)
            loc = parentloc
            parentloc = self.__parent(loc)

    def delete(self, loc):
        '''
        This method is used to delete elements from the heap at the
        required position and simply pushes up all other elements
        :param loc: position of element to be deleted in heap
        :return: None
        '''
        # position of left child
        childpos = 2*loc + 1
        if childpos >= self.size:
            while loc < self.size-1:
                # keep sliding all elements behind current position
                self.data[loc] = self.data[loc+1]
                self.data[loc].setindex(self.param, loc)
                loc += 1
            # pop element
            self.data.pop(loc)
            self.size -= 1
            return

        # position of left child
        while childpos < self.size:
            # right child position
            rightpos = childpos + 1
            # determine which child should come up
            if rightpos < self.size and not self.above_fn(self.data[childpos].getvalue(self.param), \
                                                          self.data[rightpos].getvalue(self.param)):
                childpos = rightpos
            # shift required child up
            self.data[loc] = self.data[childpos]
            # update index of node
            self.data[loc].setindex(self.param, loc)
            loc = childpos
            childpos = 2*loc + 1
        self.data.pop(loc)
        self.size -= 1

    def __bubbleDown(self,loc):
        '''
        Starts from the given location and moves the item at that spot
        as far down the heap as necessary
        :param loc: Place to start bubbling from
        '''
        self.data[loc].setindex(self.param, loc)
        swapLoc = self.__top(loc)
        while swapLoc != loc:
            (self.data[loc], self.data[swapLoc]) = \
                      (self.data[swapLoc], self.data[loc])
            self.data[loc].setindex(self.param, loc)
            self.data[swapLoc].setindex(self.param, swapLoc)
            loc = swapLoc
            swapLoc = self.__top(loc)

    def __top(self,loc):
        '''
        Finds the value among loc and loc's two children that should be at
        the top. Correctly handles end-of-heap issues.
        :param loc: Index
        :return: index of top value
        '''
        left_pos = self.__left(loc)
        if left_pos >= self.size:
            # No children: no choice!
            return loc

        this_val = self.data[loc].getvalue(self.param)
        left_val = self.data[left_pos].getvalue(self.param)
        right_pos = self.__right(loc)
        if right_pos >= self.size:
            # Choose between this one and left child
            if self.above_fn( left_val, this_val ):
                return left_pos
            else:
                return loc

        right_val = self.data[right_pos].getvalue(self.param)

        if self.above_fn( left_val, this_val ):
            if self.above_fn( left_val, right_val ):
                return left_pos
            else:
                return right_pos
        else:
            if self.above_fn( this_val, right_val ):
                return loc
            else:
                return right_pos

    def insert(self,item):
        '''
        Inserts an item into the heap.
        :param item: Item to be inserted
        '''
        self.data.append(item)
        # Note that the above line actually extends the data list.
        self.size += 1
        self.__bubbleUp(self.size-1)

    def pop(self):
        '''
        Removes and returns top of the heap
        :return: Item on top of the heap
        '''
        #if self.size <= 0:
        #    return None
        if self.size <= 0:
            return None
        result = self.data[0].popvalues()
        self.size -= 1
        if self.size > 0:
            self.data[0] = self.data.pop(self.size)
            # Note that the above line actually shrinks the data list.
            self.__bubbleDown(0)
        return result

    def __bool__(self):
        '''
        Defining thid method allows a heap to be used as a boolean value!
        See the while loops in the test code.
        :return: size of heap
        '''
        return self.size > 0

    def __str__(self):
        return "(" + str(self.size) + ")" + str(self.data)
