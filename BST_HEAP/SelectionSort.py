__author__ = 'root'
def selection_sort(list):

    for last in range(len(list)-1, 1, -1):
        pos = 0
        for max in range(1,last):
            if list[max] > list[pos]:
                pos = max
        temp = list[pos]
        list[pos] = list[last]
        list[last] = temp
    return list

def main():
    list  = [2,9,6,1,5,0,7,8,4,3]
    print(list)
    print(selection_sort(list))

main()