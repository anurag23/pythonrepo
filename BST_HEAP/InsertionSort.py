__author__ = 'root'

def insertion_sort(list):
    length = len(list)

    for index in range (1, length):
        data = list[index]
        count = index
        while data < list[count-1] and count > 0:
            list[count] = list[count-1]
            count -= 1
        list[count] = data
    return list

def main():
    list  = [2,9,6,1,5,0,7,8,4,3]
    print(list)
    print(insertion_sort(list))

main()