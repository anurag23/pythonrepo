__author__ = 'root'

class BTNode(object):

    __slots__ = ("value","left","right")

    def __init__(self, value, left = None, right = None):
        self.left = left
        self.right = right
        self.value = value

    def __str__(self):
        return str(self.value)

    def remove(self, parent, value):
        if self.value == value:
            if self.left is not None and self.right is not None:
                min = self.right.minVal()
                self.value = min
                self.right.remove(self, min)
            elif parent.left == self:
                if self.left is None:
                    parent.left = self.right
                else:
                    parent.left = self.left
            else:
                if self.left is None:
                    parent.right = self.right
                else:
                    parent.right = self.left
            return True
        elif value < self.value:
            if self.left is None:
                return False
            return self.left.remove(self, value)
        else:
            if self.right is None:
                return False
            return self.right.remove(self, value)

    def minVal(self):
        if self.left is None:
            return self.value
        else:
            return self.left.minVal()

    def inorder(self):
        return self.__inorder()
        print()

    def __inorder(self):
        res = "(" + ' '
        if self.left is not None : res += self.left.__inorder()
        res += str(self.value) + ' '
        if self.right is not None : res += self.right.__inorder()
        res += ")" + ' '
        return res

    def __eq__(self, other):
        return type(other) is BTNode and \
            self.value == other.value and \
            self.left == other.left and \
            self.right == other.right

def testBTNode():
    """
    A test function for BTNode.
    :return: None
    """
    tree1 = BTNode( 30, BTNode( 10, BTNode( 40 ) ), BTNode( 20 ) )
    tree2 = BTNode( 30, BTNode( 10, BTNode ("Forty")), BTNode( 20 ) )

    print( tree1 == tree2 )

    print(tree2.inorder())

if __name__ == '__main__':
    testBTNode()