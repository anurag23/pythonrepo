__author__ = 'root'

def merge_sort(list):
    if len(list) == 1:
        return list
    mid = len(list)//2
    return merge_list(merge_sort(list[:mid]), merge_sort(list[mid:]))

def merge_list(lista, listb):
    lena = len(lista)
    lenb = len(listb)
    aindex = 0
    bindex = 0
    res = []

    while aindex < lena and bindex < lenb:
        if lista[aindex] < listb[bindex]:
            res.append(lista[aindex])
            aindex += 1
        else:
            res.append(listb[bindex])
            bindex += 1

    if aindex < lena:
        res.extend(lista[aindex:])
    elif bindex < lenb:
        res.extend(listb[bindex:])

    return res

def main():
    list  = [2,9,6,1,5,0,7,8,4,3]
    print(list)
    print(merge_sort(list))

main()