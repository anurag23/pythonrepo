__author__ = 'root'
from BSTNode import BTNode

class BST(object):

    __slots__ = 'root', 'treesize'

    def __init__(self):
        self.root = None
        self.treesize = 0

    def insert(self, value):
        newNode = BTNode(value)
        if self.root is None:
            self.root = newNode
            self.treesize += 1
        else:
            self.__insert(value, self.root)

    def __insert(self, value, node):
        if value < node.value:
            if node.left is not None:
                self.__insert(value, node.left)
            else:
                node.left = BTNode(value)
                self.treesize += 1
        elif value > node.value:
            if node.right is not None:
                self.__insert(value, node.right)
            else:
                node.right = BTNode(value)
                self.treesize += 1

    def __str__(self):
         return self.__inorder(self.root)

    def __inorder(self, node):
        if node is None:
            return ""
        else:
            return self.__inorder(node.left) +\
                    str(node.value) +  " " + \
                self.__inorder(node.right)

    def contains(self, value):
        return self.__contains(self.root, value)

    def __contains(self, node, value):
        if node is None:
            return False
        elif node.value == value:
            return True
        elif value < node.value:
            return self.__contains(node.left, value)
        else:
            return self.__contains(node.right, value)

    def size(self):
        return self.__size(self.root)

    def __size(self, node):
        if node is None:
            return 0
        else:
            return 1 + self.__size(node.left) + self.__size(node.right)

    def height(self):
        return self.__height(self.root)

    def __height(self, node):
        if node is None:
            return -1
        else:
            return 1 + max(self.__height(node.left), self.__height(node.right))

    def remove(self, value):
        if self.root is None :
            return
        else:
            if self.root.value == value:
                if self.root.left is None:
                    temp = self.root.right
                    self.root.right = None
                    self.root = temp
                    self.treesize -= 1
                    return
                elif self.root.right is None:
                    temp = self.root.left
                    self.root.left = None
                    self.root = temp
                    self.treesize -= 1
                    return
            if(self.root.remove(None, value)):
                self.treesize -= 1

def testBST():
    """
    Test function for the binary search tree.
    :return: None
    """
    # empty tree
    t0 = BST()
    print('t0:', t0)
    print('t0 size (0):', t0.size())
    print('t0 contains 10 (False)?', t0.contains(10))
    print('t0 height (-1)?', t0.height())

    # single node tree
    t1 = BST()
    t1.insert(10)
    print('t1:', t1)
    print('t1 size (1):', t1.size())
    print('t1 contains 10 (True)?', t1.contains(10))
    print('t1 contains 0 (False)?', t1.contains(0))
    print('t1 height (0)?', t1.height())

    # tree with a parent (20), left child (10) and right child (30)
    t2 = BST()
    for val in (20, 10, 30): t2.insert(val)
    print('t2:', t2)
    print('t2 size (3):', t2.treesize)
    print('t2 contains 30 (True)?', t2.contains(30))
    print('t2 contains 0 (False)?', t2.contains(0))
    print('t2 height (1)?', t2.height())

    # a larger tree
    t3 = BST()
    for val in (17, 5, 35, 2, 16, 29, 38, 19, 33): t3.insert(val)
    print('t3:', t3)
    t3.remove(17)
    # t3.remove(2)
    t3.remove(38)
    print('t3:', t3)
    print('t3 size (6):', t3.treesize)
    print('t3 contains 16 (True)?', t3.contains(16))
    print('t3 contains 2 (False)?', t3.contains(2))
    print('t3 height (3)?', t3.height())

    #for x in 2,4,15,16,17,18,20,30,34,37,39:
    #    print( "#Nodes <", x, "=", t3.num_less( x ) )

if __name__ == '__main__':
    testBST()