from unittest import TestCase, main
from hashmap_open import HashMap
# from hashmap_open_resizable import HashMap
# from hashmap_chained import HashMap

class TestHMap( TestCase ):

    # HashMap = None # This variable is set in the main test loop.

    def setUp( self ):
        """Insert values into map. This is done for each test."""
        self.map = HashMap( 2 )
        self.numbers = 1, 11, 21, 4, 7, 10, 107, 44, 54, 0
        for x in self.numbers:
            self.map.put( x, "#" + str( x ) )

    def tearDown( self ):
        """Report on collisions."""
        print( "There were", self.map.collisions, "collisions." )

    def test_keys( self ):
        """Make sure the correct keys were put in the map."""
        assert self.map.size == len( self.numbers )
        for n in range( min( self.numbers ), max( self.numbers ) + 1 ):
            self.assertEqual( self.map.contains( n ), n in self.numbers )

    def test_values( self ):
        """Make sure the correct values were put in the map."""
        for n in range( min( self.numbers ), max( self.numbers ) + 1 ):
            if self.map.contains( n ):
                value = "#" + str( n )
                self.assertEqual( self.map.get( n ), value )

    def test_delete( self ):
        """
        Make sure values are deleted properly from the map.
        """
        originals = self.numbers # Save the input data for future tests.
        self.numbers = set( self.numbers ) # Make a copy for this test.
        nums = self.numbers
        size = len( nums )
        for n in originals:
            nums.discard( n )
            self.map.delete( n )
            size -= 1
            self.assertEquals( self.map.size, size )
            if size > 0:
                self.test_keys()
        self.numbers = originals # Restore the originals.


if __name__ == '__main__':
    main( )
