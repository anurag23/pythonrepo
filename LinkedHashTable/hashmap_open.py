"""
description: open addressing Hash Map
file: hashtable.py
language: python3
author: sps@cs.rit.edu Sean Strout
author: anh@cs.rit.edu Arthur Nunes-Harwitt
author: jsb@cs.rit.edu Jeremy Brown
author: as@cs.rit.edu Amar Saric
author: jeh@cs.rit.edu James Heliotis
"""

import map

class HashMap( map.Map ):
    """
    This is a hash map of fixed size where collisions are handled through
    open addressing probes. Once the map is full, any further calls to
    put(k,v) cause an exception to be raised.
    """
    __slots__ = 'table', 'capacity', 'collisions'

    def __init__( self, capacity=100 ):
        map.Map.__init__( self )
        self.table = capacity * [ None ]
        self.capacity = 2 if capacity < 2 else capacity
        self.collisions = 0

    def __str__( self ):
        result = ""
        for i in range( len( self.table ) ):
            e = self.table[ i ]
            if e is not None:
                result += str( i ) + ": "
                result += str( e ) + "\n"
        return result

    class Entry( object ):
        """
        A class used to hold key/value pairs.
        """

        __slots__ = 'key', 'value'

        def __init__( self, key, value ):
            """
            Create a new entry based on the provided key and value.
            """
            self.key= key
            self.value = value

        def __str__( self ):
            """
            :return: a string representation of the entry.
            """
            return "(" + str( self.key ) + ", " + str( self.value ) + ")"

    # Create an entry indicating that the search for a key should continue
    # even though there is no longer a valid entry in a certain spot, due to a
    # former deletion. Because of how the methods are written, this special
    # entry must have a key that matches no other key in the program.

    class _other: pass
    SKIP = Entry( _other(), None )

    def keys( self ):
        result = [ ]
        for entry in self.table:
            if entry is not None and entry is not HashMap.SKIP:
                result.append( entry.key )
        return result

    def contains( self, key ):
        index = hash_function( key, len( self.table ) )
        startIndex = index  # Remember where we start.*
        while self.table[ index ] is not None \
                and self.table[ index ].key != key:
            index = (index + 1) % len( self.table )
            if index == startIndex: # *We've gone all the way around!
                return False
        return self.table[ index ] is not None

    def put( self, key, value ):
        index = hash_function( key, len( self.table ) )

        # bookkeeping
        if self.table[ index ] is not None and \
                self.table[ index ].key != key:
            self.collisions += 1

        startIndex = index
        while self.table[ index ] is not None and \
                self.table[ index ] is not HashMap.SKIP and \
                self.table[ index ].key != key:
            index = (index + 1) % len( self.table )
            if index == startIndex:
                raise Exception( "Hash map is full." )
        if self.table[ index ] is None or self.table[ index ] is HashMap.SKIP:
            self.table[ index ] = HashMap.Entry( key, value )
            self.size += 1
        else:
            self.table[ index ].value = value
        return True

    def delete( self, key ):
        index = hash_function( key, len( self.table ) )
        startIndex = index
        while self.table[ index ] is not None and \
                self.table[ index ].key != key:
            index = (index + 1) % len( self.table )
            if index == startIndex:
                raise Exception( "Element to delete does not exist." )
        if self.table[ index ] is None:
            raise Exception( "Element to delete does not exist." )
        else:
            self.table[ index ] = HashMap.SKIP
            self.size -= 1

    def get( self, key ):
        index = hash_function( key, len( self.table ) )
        startIndex = index
        while self.table[ index ] is not None and \
                        self.table[ index ].key != key:
            index = ( index + 1 ) % len( self.table )
            if index == startIndex:
                raise Exception( "Hash map does not contain key." )
        if self.table[ index ] is None:
            raise Exception( "Hash map does not contain key:", key )
        else:
            return self.table[ index ].value

def hash_function( value, n ):
    """
    hash_function: NatNum -> NatNum
    Note: this is not a standard hash function because its value is limited
    by a second parameter.
    :parameter value: value to be hashed
    :parameter n: the exclusive upper limit of the result
    :return: an integer in [0 ... n)
    """
    hashcode = hash( value ) % n
    # hashcode = 0
    # hashcode = len( value ) % n
    return hashcode
