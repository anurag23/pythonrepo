""" 
file: tests.py
description: Verify the LinkedHashTable class implementation
"""

__author__ = [ "Anurag Malik", "Shraddha Pandya" ]

from linkedhashtable import LinkedHashTable

def print_set( a_set ):
    """
    Get iterator on the LinkedHashSet and print all values
    """
    for word in a_set:
        print( word, end=" " )
    print()

def test1():
    """
    This test for LinkedHashTable performs the following tests :
        1.) entering multiple entries, leading to rehashing and
                                        expansion of initial hash table.
        2.) Try entering duplicate values into the table
        3.) Remove multiple entries from the table.
        4,) Iterate over the LinkedHashTable, before and after step 3.
    :return: None
    """
    print("Test 1 :")
    table = LinkedHashTable(10)
    table.add( "This" )
    table.add( "is" )
    table.add( "test" )
    table.add( "one" )
    table.add( "for" )
    table.add( "checking" )
    table.add( "the" )
    table.add( "hashset" )
    table.add( "entering" )
    table.add( "new" )
    table.add( "data" )
    table.add( "into" )
    table.add( "the" )
    table.add( "set" )

    # duplicate values
    table.add( "set" )
    table.add ( "for" )
    table.add ( "This" )
    table.add ( "test" )


    print_set( table )
    print("Size (13) : " + str(table.size))

    print( "'This' in table? (True)", table.contains( "This" ) )
    print( "'is' in table? (True)", table.contains( "is" ) )
    print( "'test' in table? (True)", table.contains( "test" ) )
    print( "'one' in table? (True)", table.contains( "one" ) )
    print( "'into' in table? (True)", table.contains( "into" ) )
    print( "'the' in table? (True)", table.contains( "the" ) )
    print( "'set' in table? (True)", table.contains( "set" ) )

    print('\nRemoving some data')
    table.remove( "entering" )
    table.remove( "new" )
    table.remove( "data" )
    table.remove( "into" )
    table.remove( "the" )
    table.remove( "set" )

    print_set( table )
    print("Size (7) :" + str(table.size))

    print( "'into' in table? (False)", table.contains( "into" ) )
    print( "'the' in table? (False)", table.contains( "the" ) )
    print( "'set' in table? (False)", table.contains( "set" ) )



def test2():
    """
    This test for LinkedHashTable tries the following tests :
        1.) Enter multiple values into the LinkedHashTable and
                    check if the table contains some values in it or not.
        2.) Remove all the entries from the table not in order of entry,
                    and then check table size
        3.) Recheck if the table contains some values in it or not.
    :return: None
    """
    print("\nTest 2 : ")
    table = LinkedHashTable()
    table.add( "This" )
    table.add( "is" )
    table.add( "test" )
    table.add( "2" )
    table.add( "for" )
    table.add( "checking" )
    table.add( "the" )
    table.add( "hashset" )

    print_set( table )
    print("Size (8) : " + str(table.size))
    print( "'2' in table? (True)", table.contains( "2" ) )
    print( "'three' in table? (False)", table.contains( "three" ) )
    print( "'hashset' in table? (True)", table.contains( "hashset" ) )


    table.remove( "is" )
    table.remove( "hashset" )
    table.remove( "test" )
    table.remove( "This" )
    table.remove( "checking" )
    table.remove( "the" )
    table.remove( "2" )
    table.remove( "for" )

    print_set( table )
    print("Size (0) : " + str(table.size))
    print( "'2' in table? (False)", table.contains( "2" ) )
    print( "'three' in table? (False)", table.contains( "three" ) )
    print( "'hashset' in table? (False)", table.contains( "hashset" ) )

def test3():
    """
    This method performs the following tests on LinkedHashTable:
        1.) Add multiple entries into the LinkedHashTable and iterate
                        on them in their order of entry.
        2.) Remove some elements from the table and check contains
        3.) Try to remove a wrong element from the table
            that doesn't exist in the HashTable. Exception must be raised.
    :return: None
    """
    print("\nTest 3 : ")
    table = LinkedHashTable(1)
    table.add( "This" )
    table.add( "is" )
    table.add( "test" )
    table.add( "three" )

    print_set(table)
    print("Size (4) : " + str(table.size))
    print( "'This' in table? (True)", table.contains( "This" ) )
    print( "'three' in table? (True)", table.contains( "three" ) )
    print( "'hashset' in table? (False)", table.contains( "hashset" ) )


    table.remove( "test" )
    table.remove( "is" )
    table.remove( "This" )

    print()
    print_set( table )
    print("Size (1) : " + str(table.size))
    print( "'This' in table? (False)", table.contains( "This" ) )
    print( "'three' in table? (True)", table.contains( "three" ) )
    print( "'hashset' in table? (False)", table.contains( "hashset" ) )

    print("\nNow trying to remove a wrong element. Expecting exception :")
    table.remove("four")


if __name__ == '__main__':
    test1()
    test2()
    test3()


