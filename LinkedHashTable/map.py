""" 
file: XXX.py
language: python3
description: XXX
"""
__author__ = 'James Heliotis'

from abc import abstractmethod, ABCMeta


class Map( metaclass=ABCMeta ):
    """
    Maps represent a collection of key-value associations.
    In the documentation below we refer to a 'Map<K,V>', where
    'K' is the key type and 'V' is the value type.
    No two values may have the same key, but more than one
    key may have the same value.
    """

    __slots__ = 'size'

    def __init__( self ):
        """
        This abstract class just keeps the state information about the
        'size', or number of entries in the map.
        (So, all maps must manually maintain their number of entries.)
        """
        self.size = 0

    @abstractmethod
    def keys( self ):
        """
        keys: Map<K,V> -> List<K>
        Return a list of keys in the given hashMap.
        """
        pass

    @abstractmethod
    def contains( self, key ):
        """
        has: Map<K,V>, K -> bool
        Return True iff this map has an entry with the given key.
        """
        pass

    @abstractmethod
    def put( self, key, value ):
        """
        put: Map<K,V>, K, V -> NoneType

        Set the given key to the given value. If the key already exists,
        the given value will replace the previous one already in the map;
        otherwise a new entry is added to the map and the map's size
        increases by 1.
        If the map is full, an Exception is raised.
        """
        pass

    @abstractmethod
    def delete( self, key ):
        """
        delete: Map<K,V>, K -> NoneType
        Remove the entry identified by key from this map. The size of the
        map decreases by 1.
        :pre: self.contains( key )
        """
        pass

    @abstractmethod
    def get( self, key ):
        """
        get: Map<K,V>, K -> V
        Return the value associated with the given key in this map.
        Precondition: self.contains( key )
        """
        pass

