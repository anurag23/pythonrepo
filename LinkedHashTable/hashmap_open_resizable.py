"""
description: open addressing Hash Map with resizing
file: hashtable_open.py
language: python3
author: sps@cs.rit.edu Sean Strout
author: anh@cs.rit.edu Arthur Nunes-Harwitt
author: jsb@cs.rit.edu Jeremy Brown
author: as@cs.rit.edu Amar Saric
author: jeh@cs.rit.edu James Heliotis
"""

import map

class HashMap( map.Map ):
    """
    This open-addressing HashMap data structure contains a list of values
    where each value is in an array 'table' and located by a hashable key.
    When the table fills up, a larger table is allocated and all of the
    current entries are transferred to it. This is called 'rehashing'.
    """
    __slots__ = 'table', 'capacity', 'collisions'

    def __init__( self, capacity=100 ):
        map.Map.__init__( self )
        self.table = capacity * [ None ]
        self.capacity = 2 if capacity < 2 else capacity
        self.collisions = 0

    def __str__( self ):
        result = ""
        for i in range( len( self.table ) ):
            e = self.table[ i ]
            if e is not None:
                result += str( i ) + ": "
                result += str( e ) + "\n"
        return result

    class Entry( object ):
        """
        A class used to hold key/value pairs.
        """

        __slots__ = 'key', 'value'

        def __init__( self, key, value ):
            self.key= key
            self.value = value

        def __str__( self ):
            return "(" + str( self.key ) + ", " + str( self.value ) + ")"

    # Create an entry indicating that the search for a key should continue
    # even though there is no valid entry in a certain spot due to a deletion.
    # The entry must have a key that matches no other key in the program.

    class _other: pass
    SKIP = Entry( _other(), None )

    def keys( self ):
        result = [ ]
        for entry in self.table:
            if entry is not None and entry is not HashMap.SKIP:
                result.append( entry.key )
        return result

    def contains( self, key ):
        index = hash_function( key, len( self.table ) )
        startIndex = index  # Remember where we start.*
        while self.table[ index ] is not None \
                and self.table[ index ].key != key:
            index = (index + 1) % len( self.table )
            if index == startIndex: # *We've gone all the way around!
                return False
        return self.table[ index ] is not None

    def put( self, key, value ):
        if self.size == self.capacity:
            self._rehash()
        index = hash_function( key, len( self.table ) )

        # bookkeeping
        if self.table[ index ] is not None and \
                self.table[ index ].key != key:
            self.collisions += 1

        startIndex = index
        while self.table[ index ] is not None and \
                self.table[ index ] is not HashMap.SKIP and \
                self.table[ index ].key != key:
            index = (index + 1) % len( self.table )
            if index == startIndex:
                raise Exception( "Hash map is full." )
        if self.table[ index ] is None or self.table[ index ] is HashMap.SKIP:
            self.table[ index ] = HashMap.Entry( key, value )
            self.size += 1
        else:
            self.table[ index ].value = value
        return True

    def delete( self, key ):
        index = hash_function( key, len( self.table ) )
        startIndex = index
        while self.table[ index ] is not None and \
                self.table[ index ].key != key:
            index = (index + 1) % len( self.table )
            if index == startIndex:
                raise Exception( "Element to delete does not exist." )
        if self.table[ index ] is None:
            raise Exception( "Element to delete does not exist." )
        else:
            self.table[ index ] = HashMap.SKIP
            self.size -= 1

    def get( self, key ):
        index = hash_function( key, len( self.table ) )
        startIndex = index
        while self.table[ index ] is not None and \
                        self.table[ index ].key != key:
            index = ( index + 1 ) % len( self.table )
            if index == startIndex:
                raise Exception( "Hash map does not contain key." )
        if self.table[ index ] is None:
            raise Exception( "Hash map does not contain key:", key )
        else:
            return self.table[ index ].value

    def _rehash( self ):
        """
        Rebuild the map in a larger table. The current map is not changed
        in any way that can be seen by its clients, but internally its table is
        grown.
        :return: None
        """
        new_cap = 2 * self.capacity
        print( "Rehashing from", self.capacity, "to", new_cap )
        new_map = HashMap( new_cap )
        for key in self.keys():
            new_map.put( key, self.get( key ) )
        self.capacity = new_map.capacity
        self.table = new_map.table

def hash_function( value, n ):
    """
    hash_function: NatNum -> NatNum
    :parameter value: value to be hashed
    :return: an integer in [0 ... n)
    """
    # hashcode = hash( value ) % n
    hashcode = 0
    # hashcode = len( value ) % n
    return hashcode
