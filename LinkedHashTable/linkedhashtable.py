__author__ = 'Anurag Malik', 'Shraddha Pandya'

from set import SetType
from collections.abc import Iterator

class ChainNode(object):

        """
        This class represents a node in the slot linked list for a hash-table.
        A key only entry with link to next element in a hashtable slot and
        links to previous and next in order elements.
        """
        __slots__ = 'obj', 'chain', 'prev', 'link'

        def __init__( self, obj, prev=None, link=None, chain=None ):
            """
            Initialize new node of the chained list with the input values.

            :param obj: object to be stored in this node
            :param chain : link to next node in the hashtable slot
            :param prev : link to the previous element in the order of insertion into hash table
            :param next : link to the next element in the order of insertion into hash table
            """
            self.obj = obj
            self.chain = chain
            self.prev = prev
            self.link = link

        def __str__(self):
            return str(self.obj)

class LinkedHashTable(SetType):
    """
    This class represents a LinkedHashTable capable for storing elements
    in their order of entry into the hash table.This hash table if chained and
    resolves any collisions by storing new elements into linked lists in its slots.
    """

    __slots__ = 'table', 'size', 'front', 'back', 'capacity', 'load_factor'

    def __init__(self, initial_num_buckets=100, load_limit=0.75):

        '''
        Create a new empty hash table.
        :param initial_num_buckets: starting number_of_buckets
        :param load_limit: See class documentation above.
        :return: None
        '''
        SetType.__init__( self)
        if initial_num_buckets < 10:
            initial_num_buckets = 10
        self.table = [ None ] * initial_num_buckets
        self.capacity = initial_num_buckets
        self.load_factor = load_limit
        self.front = None
        self.back = None
        self.size = 0


    def __iter__(self):
        """
        Build an iterator.
        :return: an iterator for the current elements in the set
        """
        return LinkedHashTable.HashTableIterator(self.front)

    class HashTableIterator(Iterator):
        """
        This internal class represents a Iterator for the parent LinkedHashTable.
        It allows applications to traverse all the elements stored in hashtable in
        the order they were inserted.

        :param : curRec : current record to be returned by the iterator
        :param : nextRec : next record to be returned by the iterator
        """
        __slots__ = 'curRec', 'nextRec'

        def __init__(self, start):
            """
            Initialize new iterator instance to the start of linked list
            of elements in order they were entered.
            """
            self.nextRec = start

        def __next__(self):
            """
            This method returns the value of next node to be traversed by
            an iterator instance. It returns back the value of current node
            being iterated and then update the next node.
            """
            record = self.nextRec
            if record is None:
                # stop iteration if there are
                # no more elements to traverse
                raise StopIteration()
            self.nextRec = record.link
            self.curRec = record
            return record.obj


    def add(self, obj):
        """
        Insert a new object into the hash table and remember when it was added
        relative to other calls to this method. However, if the object is
        added multiple times, the hash table is left unchanged, including the
        fact that this object's location in the insertion order does not change.
        Double the size of the table if its load_factor exceeds the load_limit.
        :param obj: the object to add
        :return: None
        """
        index = self.hash_function(obj)

        # rehash and expand the current hash-table
        # if the number of elements has led to loadfactor
        # more than the required load factor - 0.75 default.
        if self.size // self.capacity >= self.load_factor:
            self._rehash()
            index = self.hash_function(obj)

        newRecord = ChainNode(obj)
        if self.table[index] is None:
            # add element to empty index in table
            self.table[index] = newRecord
        else:
            # add element to the back of the
            # link at an index in table
            record = self.table[index]
            while record.chain is not None:
                if record.obj == obj:
                    return
                record = record.chain
            if record.obj == obj:
                return
            record.chain = newRecord

        self.size += 1
        if self.front is None:
            self.front = newRecord
        newRecord.prev = self.back
        if self.back is not None:
            self.back.link = newRecord
        self.back = newRecord

    def _rehash(self, expand=True):

        """
        This method is responsible for rehashing the table data
        into larger or shorter hash tables. a boolean parameter is
         used to decide if the original hash table has to be expanded
         by 2 or shorted by 2.
        :param expand: TRUE if hash table should be expanded twice its current capacity
                       FALSE if hash table should be shortened half of its current capacity
        :return: None
        """
        if expand:
            new_cap = 2 * self.capacity
        else:
            new_cap = self.capacity // 2
        if new_cap < 10:
            new_cap = 10
        new_map = LinkedHashTable(new_cap)
        for record in self:
            new_map.add(record)
        self.capacity = new_map.capacity
        self.table = new_map.table
        self.front = new_map.front
        self.back = new_map.back

    def hash_function(self, key):
        """
        This method returns the hash code for any input key item
        :param key: data whose hash code is to be generated
        :return: hash code (int)
        """
        return hash(key) % self.capacity

    def contains(self, obj):
        """
        Is the given obj in the hash table?
        :return: True iff obj or its equivalent has been added to this table
        """
        index = self.hash_function(obj)
        record = self.table[index]
        while record is not None:
            if record.obj == obj:
                return True
            record = record.chain
        return False

    def remove(self, obj):
        """
        Remove an object from the hash table (and from the insertion order).
        Resize the table if its size has dropped below
        (1-load_factor)*current_size.
        :param obj: the value to remove; assumes hashing and equality work
        :return:
        """
        index = self.hash_function(obj)

        # rehash the elements in hash table, and shorten the
        # current hash table to half of its current size
        if self.size // self.capacity <= (1-self.load_factor):
            self._rehash(False)
            index = self.hash_function(obj)

        record = self.table[index]
        if record is None:
            # no element exists at associated index in table
            raise Exception( "Element '"+str(obj)+"' to be removed does not exist." )
        if record.obj == obj:
            # remove if the element is found at index of table
            self.table[index] = record.chain
            cursor = record
        else:
            # traverse the list at index, search required obj
            # and remove it from the linked list
            link = record.chain
            current = record
            found = False
            while link is not None:
                if link.obj == obj:
                    current.chain = link.chain
                    cursor = link
                    found = True
                    break
                current = link
                link = link.chain

            if not found:
                raise Exception("Element '"+str(obj)+"' to be removed does not exist.")

        self.size -= 1
        # remove the required object from the linked list of elements
        # stored in order of their entry into the hash table
        if cursor == self.front:
            self.front = cursor.link
        if cursor == self.back:
            self.back = cursor.prev
        if cursor.prev is not None:
            cursor.prev.link = cursor.link
        if cursor.link is not None:
            cursor.link.prev = cursor.prev