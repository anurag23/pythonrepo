"""
description: chained Hash Map with resizing
file: hashtable_open.py
language: python3
author: sps@cs.rit.edu Sean Strout
author: anh@cs.rit.edu Arthur Nunes-Harwitt
author: jsb@cs.rit.edu Jeremy Brown
author: as@cs.rit.edu Amar Saric
author: jeh@cs.rit.edu James Heliotis
"""

import map

class HashMap( map.Map ):
    """
    This hash map table is a list of linked lists. Each node
    in each linked list contains an entry in the map.
    So as the table fills up more and more, a linear component to the
    search becomes more significant. This HashMap rehashes to twice its
    size when the number of entries exceeds the length of the table.
    """
    __slots__ = 'table', 'capacity', 'size', 'collisions'

    def __init__( self, capacity=100 ):
        map.Map.__init__( self )
        self.table = capacity * [ None ]
        self.capacity = 2 if capacity < 2 else capacity
        self.collisions = 0

    def __str__( self ):
        result = ""
        for i in range( len( self.table ) ):
            e = self.table[ i ]
            if e is not None:
                result += str( i ) + ": "
                result += str( e ) + "\n"
        return result

    class ChainNode( object ):
        """
        A key-value entry plus a link to make it a node in a linked list
        """

        __slots__ = 'key', 'value', 'chain'

        def __init__( self, key, value=None, chain=None ):
            self.key = key
            self.value = value
            self.chain = chain

        def __str__( self ):
            return "(" + str( self.key ) + ", " + str( self.value ) + ")"

    def keys( self ):
        result = [ ]
        for entry_list in self.table:
            entry = entry_list # Start at front
            while entry is not None:
                result.append( entry.key )
                entry = entry.chain
        return result

    def contains( self, key ):
        index = hash_function( key, len( self.table ) )
        entry = self.table[ index ]
        while entry is not None:
            if entry.key == key:
                return True
            entry = entry.chain
        return False

    def put( self, key, value ):

        index = hash_function( key, len( self.table ) )

        # bookkeeping
        if self.table[ index ] is not None and \
                self.table[ index ].key != key:
            self.collisions += 1

        if self.size == self.capacity:
            self._rehash()
            index = hash_function( key, len( self.table ) )
        front = self.table[ index ]
        entry = front
        while entry is not None:
            if entry.key == key:
                entry.value = value
                return
            entry = entry.chain
        self.table[ index ] = HashMap.ChainNode( key, value, front )
        self.size += 1

    def delete( self, key ):

        index = hash_function( key, len( self.table ) )
        front = self.table[ index ]
        if front is None:
            raise Exception( "Element to delete does not exist." )
        if front.key == key:
            self.table[ index ] = front.chain
            self.size -= 1
            return
        entry = front.chain
        prev = front
        while entry is not None:
            if entry.key == key:
                prev.chain = entry.chain
                self.size -= 1
                return
            prev = entry
            entry = entry.chain
        raise Exception( "Element to delete does not exist." )

    def get( self, key ):
        index = hash_function( key, len( self.table ) )
        entry = self.table[ index ]
        while entry is not None:
            if entry.key == key:
                return entry.value
            entry = entry.chain
        raise Exception( "Element to get does not exist." )

    def _rehash( self ):
        """
        Rebuild the map in a larger table. The current map is not changed
        in any way that can be seen by its clients, but internally its table is
        grown.
        :return: None
        """
        new_cap = 2 * self.capacity
        print( "Rehashing from", self.capacity, "to", new_cap )
        new_map = HashMap( new_cap )
        for key in self.keys():
            new_map.put( key, self.get( key ) )
        self.capacity = new_map.capacity
        self.table = new_map.table
        # (Don't copy collisions. Collisions in rehashing don't count for us.)

def hash_function( value, n ):
    hashcode = hash( value ) % n
    # hashcode = 0
    # hashcode = len( value ) % n
    return hashcode
